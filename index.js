const express = require('express');

const app = express();

app.get('/', (req, res) => {
	res.send({ hello: 'world' });
});

app.listen(4302, () => {
	console.log('app is running smooth...');
});
